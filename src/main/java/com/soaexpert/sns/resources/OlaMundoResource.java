package com.soaexpert.sns.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/")
@Consumes("*/*")
public class OlaMundoResource {
	@GET
	public Response digaOi() {
		return Response.ok("Ola, mundo!", "text/plain").build();
	}

}
